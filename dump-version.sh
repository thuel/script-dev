#!/bin/bash
#
###
# Script purpose: With this script, the version number in an other file can be changed.
# Version: 0.2
# Usage: dump-version.sh "new version number" file
###
#
# Get the scripts name
me=${0##*\/}
#
# block to generate error messages
# optionally works together with log_msg (see commented code line)
error_msg() {
	err(){
		echo "$1"
#		log_msg "$1"	
	}
	case $1 in
		1) #wrong script usage
			msg="Usage: sh $me version file OR ./$me version file OR /full/path/to/$me version file"
			err "$msg"
			;;
		2) #error description
			msg=""
			err "$msg"
			;;
		3) #error description
			msg=""
			err "$msg"
			;;
		4) #error description
			msg=""
			err "$msg"
			;;
		*) #any other error; more for development use
			msg="unknown error"
			err "$msg"
			;;
	esac
}

if [[ -z $1 || -z $2 ]]; then
	error_msg 1 && exit 1
fi
# raw implementation of the scrips purpose
sed -i "0,/^# Version:/s/^# Version:.*/# Version: $1/" $2

exit 0
