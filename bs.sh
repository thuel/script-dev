#!/bin/sh
#
###
# Script purpose: Script to build shell scripts
# Version: 0.2
# Usage: bs.sh newfile
###
#
# The script will be created in the current working directory
[ -z "$1" ] && echo "Usage: bs.sh scriptname" && exit 1
DEFAULT_VERSION="# Version: 0.1"
cat > $1 << EOF
#!/bin/sh
###
# Script purpose:
$DEFAULT_VERSION
# Usage:
###
#
EOF
cat >> $1 << 'EOF'
# Get the scripts name
me=${0##*\/}
#

EOF
chmod +x $1
editor $1
