#!/bin/bash
#
###
# Script purpose: Generate scripts to create a self extracting installer
# Version: 0.1
# Usage: 
###
#
# MAKE BUILD SCRIPT
BUILD=build
# 'Here document containing the body of the generated build script.
(
cat <<'EOF'
#
###
# Script purpose: build a selfextracting installer.
# Version: 0.1
# Usage: place this script and the scripts decompress and
#	installer in the directory with files to "package", then
#	execute the script. You can give a file name to the build
#	script. Default name of the selfextracting file would be
#	selfextract.
###
#
me=${0##*\/}
#
DEBUG="no"
#
# build file archive; exclude the installation files
TAREXC=( \
	--exclude="./build" \
	--exclude="./decompress" \
	--exclude="./payload.tar" \
	--exclude="./installer.template" \
	--exclude="./.git" \
)
tar cf ./payload.tar "${TAREXC[@]}" ./* 
#
if [ ! -z $1 ]; then
	INST="$1.bsx"
else
	INST="selfextract.bsx"
fi
#
# check if the creation of the payload.tar was successful;
#	if yes: compress it and append it to decompress
if [ -e "payload.tar" ]; then 
	gzip payload.tar 
	if [ -e "payload.tar.gz" ]; then 
		cat decompress payload.tar.gz > $INST 
	else 
		echo "payload.tar.gz does not exist" 
		exit 1
	fi 
	else 
	echo "payload.tar does not exist" 
	exit 1 
fi 
# make installer executable
chmod +x $INST

# remove the payload.tar.gz
if [ $DEBUG == "no" ]; then
	rm payload.tar.gz
fi

echo "$INST created" 
exit 0
EOF
) > $BUILD
if [ -f "$BUILD" ]
then
  chmod 755 $BUILD
  # Make the generated file executable.
else
  echo "Problem in creating file: \"$BUILD\""
fi

# MAKE DECOMPRESS SCRIPT
DECOMPRESS=decompress
# 'Here document containing the body of the generated decompress script.
(
cat <<'EOF'
#!/bin/bash
#
###
# Script purpose: The Decompression Script does most of the work. 
#    The compressed archive of the payload directory will be 
#    appended onto this script. When ran, the script will remove the 
#    archive, decompress it and execute the install script which has
#    to be placed in the payload directory.
# Version: 0.1
# Usage: place in the parent directory of the payload directory.
###
#
DEBUG="yes"
echo "" 
echo "Self Extracting Installer" 
echo "" 

export TMPDIR=$(mktemp -d /tmp/selfextract.XXXXXX)
if [ $DEBUG == "yes" ]; then
	echo "TMPDIR: $TMPDIR"
fi

ARCHIVE=$(awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0)
if [ $DEBUG == "yes" ]; then
	echo "ARCHIVE: $ARCHIVE"
fi

tail -n +$ARCHIVE $0 | tar xzv -C $TMPDIR 

CDIR=`pwd` 

cd $TMPDIR 

./installer 

cd $CDIR 

rm -rf $TMPDIR 

exit 0 

__ARCHIVE_BELOW_
EOF
) > $DECOMPRESS
if [ -f "$DECOMPRESS" ]
then
  chmod 755 $DECOMPRESS
  # Make the generated file executable.
else
  echo "Problem in creating file: \"$DECOMPRESS\""
fi

# MAKE INSTALLER SCRIPT
INSTALLER=installer
# 'Here document containing the body of the generated installer script.
(
cat <<'EOF'
#!/bin/bash 
#
###
# Script purpose: installation script that will handle the payload to install.
#    This script contains any actions that you'd wish to be performed on the 
#    installation system, make directories, uncompress files, run system commands, etc.
# Version: 0.1
# Usage: put the script in the payload and put all commands to run in this script
###
#
echo "Running Installer"

EOF
) > $INSTALLER
if [ -f "$INSTALLER" ]
then
  chmod 755 $INSTALLER
  # Make the generated file executable.
else
  echo "Problem in creating file: \"$INSTALLER\""
fi
# Add the files in the current directory in the body of the installer script
#	but exclude those specified in this array
EXCFILE=(
	! -path "./build" \
	! -path "./decompress" \
	! -path "./installer" \
	! -path "./make_installer.sh" \
	! -path "*.bsx" \
	! -path "*/\.git*" \
)
LIST=$(find . "${EXCFILE[@]}" -type f)
for file in $LIST; do
	echo "cp $file" >> $INSTALLER
done
echo "exit 0" >> $INSTALLER

echo "Scripts generated. Use ./build to generate a selfextracting installer of the files in the current directory"

exit 0
