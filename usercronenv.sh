#!/bin/bash
#
###
# Script purpose: generate cron environnment to test scripts which will be executed by cron
# Version : 0.1
# Script usage: parameter $1 is the user under which the cron job will run
###
#
# Get script name
me=${0##*\/}
#
# Debug
DEBUG="yes"
#
# Make temporary file:
tmpcrontab=$(mktemp crontab.$1.XXXXXXX)
# Store the crontab in the tmp file:
crontab -u $1 -l > $tmpcrontab
# Make a backup before modification:
tmpbackup=$(mktemp ct.$1.backup.XXXXXXX)
cp $tmpcrontab $tmpbackup
# Add a new line to the tmp file:
echo "* * * * * /usr/bin/env > /home/"$1"/.cron-env" >> $tmpcrontab
# Load the modifyied tmp file into crontab:
crontab -u $1 $tmpcrontab
#
# Actual second in this minute
SEC=$(date +%S)
# Seconds to the next full minute
SECMIN=$((62 - $SEC))
if [ $SECMIN -lt 3 ]; then
    SECMIN=62
fi
# wait till the cron job gets executed; one minute at maximum
sleep $SECMIN
if [ -e /home/"$1"/.cron-env ]; then
	chmod 0440 /home/"$1"/.cron-env
else
	echo "Generation of cron-env not successful"
	crontab -u $1 $tmpbackup
	rm -f $tmpcrontab
	rm -f $tmpbackup
	exit 1
fi
# restore the crontab
crontab -u $1 $tmpbackup
# Remove temporary files
rm -f $tmpcrontab
rm -f $tmpbackup
exit 0