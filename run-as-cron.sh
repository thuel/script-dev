#!/bin/bash
#
###
# Script purpose: Test commands in cron environnment
# Version : 0.1
# Script usage: Note that the second argument needs to be quoted if it requires an argument. 
#	The first line of the script loads a POSIX shell as interpreter. 
#	The nineteenth line sources the cron environment file. This is required to load the correct shell, 
#	which is stored in the environment variable SHELL. Then it loads an empty environment 
#	(to prevent leaking of environment variables into the new shell), launches the same shell 
#	which is used for cronjobs and loads the cron environment variables. 
#	Finally the command is executed ($2).
###
#
# Get script name
me=${0##*\/}
#

. "$1"
exec /usr/bin/env -i "$SHELL" -c ". $1; $2"