#!/bin/bash

# Script to generate the minimal structure of new python module
# Modulname may be a relative or absolute path

usage(){
    echo "$0 '<module name>'"
}

createDirTree(){
    mkdir -p $path/$mod
}

setupFiles(){
# setup.py
echo -n "Short description: "
read description
echo -n "Give some keywords: "
read keywords
echo -n "Package URL: "
read pURL
echo -n "Your name: "
read author
echo -n "Your e-mail address: "
read email
echo -n "License of the module: "
read license
setupPath=$path"/setup.py"
cat << EOF > $setupPath
from setuptools import setup, find_packages

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='$mod',
      version='0.1',
      description='$description',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: ??? :: ???',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Topic :: ??? :: ???',
      ],
      keywords='$keywords',
      url='$pURL',
      author='$author',
      author_email='$email',
      license='$license',
      packages=find_packages(),
      py_modules=[
          '$mod',
          '$mod.$mod',
      ],
      install_requires=[
          '',
      ],
      include_package_data=True,
      zip_safe=False)
EOF

#.gitignore
ignorePath=$path"/.gitignore"
cat << EOF > $ignorePath
# Compiled python modules.
*.pyc

# Setuptools distribution folder.
/dist/

# Python egg metadata, regenerated from source files by setuptools.
/*.egg-info
/*.egg
EOF

#__init__.py
initPath="$path/$mod""/__init__.py"
cat << EOF > $initPath
from $mod"."$mod".py" import ???
EOF

#MANIFEST.in
manifestPath=$path"/MANIFEST.in"
cat << EOF > $manifestPath
include README.rst
EOF

#README.rst
readmePath=$path"/README.rst"
cat << EOF > $readmePath
======
README
======

EOF

# the module
modulePath=$path/$mod/$mod".py"
cat << EOF > $modulePath
#!/usr/bin/env python


if __name__=="__main__":
    
EOF

}

makeExecutable(){
    find $path -iname *.py -exec chmod +x {} \;
}

main(){
    if [[ ! $path ]]; then
        usage
        exit 1
    fi
    createDirTree
    setupFiles
    makeExecutable
}

path=$1
mod=$(basename $1)

main
